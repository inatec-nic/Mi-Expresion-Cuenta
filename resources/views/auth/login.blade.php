@extends('layouts.app')
@section('content')
<div class="background-login">
    <section class="container-login">
        <div class="container-form">
                            <div class="form-inputs">
                                <p class=" title">Iniciar sesión</p>
                                <form class="form-horizontal form-login" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control inputs" name="email" placeholder="Correo del usuario" value="{{ old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control inputs" name="password" placeholder="Ingresa tu contraseña" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordar contraseña
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a> -->
                                    <button type="submit" class="btn btn-green btn-primary">
                                        Ingresar
                                    </button>
                                </form>
                            </div>
        </div>
    </section>
</div>
@endsection
